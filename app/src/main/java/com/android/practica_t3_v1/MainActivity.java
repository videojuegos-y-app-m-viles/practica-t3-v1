package com.android.practica_t3_v1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.android.practica_t3_v1.adapters.ItemAdapter;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    List<Contacto> listaContactos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.listarContactos);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        listaContactos = new ArrayList<>();
        llenarContactos();

        ItemAdapter adapter = new ItemAdapter(listaContactos);
        recyclerView.setAdapter(adapter);


    }
    private void llenarContactos(){
        Contacto contacto = new Contacto();
        contacto.setId(1);
        contacto.setNombre("Pablo Burgos");
        contacto.setNumero("910361163");

        listaContactos.add(contacto);

        contacto = new Contacto();
        contacto.setId(2);
        contacto.setNombre("Sthefanie Portal");
        contacto.setNumero("987162738");
        listaContactos.add(contacto);

        contacto = new Contacto();
        contacto.setId(3);
        contacto.setNombre("Jackeline Martos");
        contacto.setNumero("978273625");
        listaContactos.add(contacto);

        contacto = new Contacto();
        contacto.setId(4);
        contacto.setNombre("Pepito Chávez");
        contacto.setNumero("971625301");
        listaContactos.add(contacto);

        contacto = new Contacto();
        contacto.setId(5);
        contacto.setNombre("Cristina Briones");
        contacto.setNumero("910361163");
        listaContactos.add(contacto);

        contacto = new Contacto();
        contacto.setId(6);
        contacto.setNombre("Rodrigo Ramírez");
        contacto.setNumero("976346643");
        listaContactos.add(contacto);

        contacto = new Contacto();
        contacto.setId(7);
        contacto.setNombre("Ana Burgos");
        contacto.setNumero("901283392");

        listaContactos.add(contacto);

        contacto = new Contacto();
        contacto.setId(11);
        contacto.setNombre("Nadia Ramos");
        contacto.setNumero("987123650");
        listaContactos.add(contacto);

        contacto = new Contacto();
        contacto.setId(10);
        contacto.setNombre("Antonio Gomez");
        contacto.setNumero("908285647");
        listaContactos.add(contacto);

        contacto = new Contacto();
        contacto.setId(8);
        contacto.setNombre("Nelson Aliaga");
        contacto.setNumero("987654345");

        listaContactos.add(contacto);

        contacto = new Contacto();
        contacto.setId(9);
        contacto.setNombre("Juana Ramírez");
        contacto.setNumero("980123098");

        listaContactos.add(contacto);
    }
}