package com.android.practica_t3_v1.adapters;

import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.android.practica_t3_v1.Contacto;
import com.android.practica_t3_v1.R;

import java.util.List;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ViewHolder>{
    private List<Contacto> listaContactos;

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView nombre;
        private final TextView numero;
        private final Button btnLlamar;
        private final Button btnSMS;
        public ViewHolder(View view){
            super(view);
            nombre = (TextView) view.findViewById(R.id.txtNombre);
            numero = (TextView) view.findViewById(R.id.txtNumero);
            btnLlamar = view.findViewById(R.id.btnLLamar);
            btnSMS = view.findViewById(R.id.btnSms);

        }
        public TextView getNombre(){
            return this.nombre;
        }
        public TextView getNumero(){return this.numero;}
        public Button getBtnLlamar(){return this.btnLlamar;}
        public Button getBtnSMS(){return this.btnSMS;}
    }
    public ItemAdapter(List<Contacto> contactos){
        this.listaContactos = contactos;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType){
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.contacto_item,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        viewHolder.getNumero().setText(listaContactos.get(position).getNumero());
        viewHolder.getNombre().setText(listaContactos.get(viewHolder.getAdapterPosition()).getNombre());


        viewHolder.getBtnLlamar().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", listaContactos.get(viewHolder.getAdapterPosition()).getNumero(), null));
                view.getContext().startActivity(intent);
            }
        });
        viewHolder.getBtnSMS().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*
                Intent smsIntent = new Intent(android.content.Intent.ACTION_VIEW);
                smsIntent.setType("vnd.android-dir/mms-sms");
                smsIntent.putExtra("address",listaContactos.get(viewHolder.getAdapterPosition()).getNumero());
                smsIntent.putExtra("sms_body","Enviar Mensaje a " + listaContactos.get(viewHolder.getAdapterPosition()).getNombre());
                smsIntent.setFlags(android.content.Intent.FLAG_ACTIVITY_NEW_TASK);
                view.getContext().startActivity(smsIntent);*/

                Intent sendIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + listaContactos.get(viewHolder.getAdapterPosition()).getNumero()));
                sendIntent.putExtra("sms_body", "Enviar Mensaje a " + listaContactos.get(viewHolder.getAdapterPosition()).getNombre());
                view.getContext().startActivity(sendIntent);
            }
        });
    }

    @Override
    public int getItemCount(){
        return listaContactos.size();
    }
}